FROM influxdb
COPY influxdb.conf /etc/influxdb/influxdb.conf

EXPOSE 8083/tcp
EXPOSE 8086/tcp
EXPOSE 25826/tcp

ENTRYPOINT ["/entrypoint.sh"]
CMD ["influxd"]
